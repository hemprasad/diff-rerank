This is a preliminary implementation for the reranking algorithm proposed in 

Re-ranking by Multi-feature Fusion with Diffusion for Image Retrieval
Fan Yang, Bogdan Matei and Larry S. Davis
IEEE Winter Conference on Applications of Computer Vision (WACV), 2015. 

Run demo.m to see how it works. You need to download and install the diffusion package from http://vh.icg.tugraz.at/index.php?content=topics/diffusion.php.

If you would like to run on your own datasets, you have to pre-compute the similarity/distance matrices for dataset images in advance and save them to a mat file. Another mat file containing the similarity/distance matrices between dataset images and images of an irrelevant set is also needed. Each row in the matrix represents the similarity/distance between a dataset image and all other dataset/irrlevant images.

To evaluate the re-ranking results on the Holidays dataset shown as an example, you need to use the evaluation package from http://lear.inrialpes.fr/~jegou/data.php.  

Any questions please contact Fan Yang (fyang@umiacs.umd.edu)
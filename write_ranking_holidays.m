function write_ranking_holidays(files, ranking, name)
%
fid = fopen(name, 'w');
for i = 1:length(files)
    if strcmp(files{i}(5:6), '00')
        rl = ranking(i,:);
        fprintf(fid, '%s ', [files{i} '.jpg']);
        for j = 1:length(rl)
            if rl(j)>1491
                rname = 'distractor';
            else
                rname = [files{rl(j)} '.jpg'];
            end
                
            if strcmp(rname(1:4), files{i}(1:4))
                fprintf(fid, '%d %s ', j-1, rname);
            end
        end
        fprintf(fid, '\n');
    end
end
fclose(fid);